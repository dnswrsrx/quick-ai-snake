from random import randint 


height = width = 5
py_length = height - 1


class Grid():

    grid = [[0 for row in range(height)] for col in range(width)]

    def place(coord, object):
        Grid.grid[coord[0]][coord[1]] = object
        return(Grid.grid)
    
    def replace(coord, replacement):
        Grid.grid[coord[0]][coord[1]] = replacement
        return(Grid.grid)



class Object:
    
    food = []
    snake = [
            [],
            [],
            []
            ]

    symbol = {'snake': 's', 'food': 'f', 'cell': 0}
    

class Snake_Movement:

    def head(hd, to):
        dy = to[0] - hd[0][0]
        dx = to[1] - hd[0][1]

        if dy > 0:
            return([hd[0][0] + 1, hd[0][1]])
        elif dy < 0:
            return([hd[0][0]  - 1, hd[0][1]])
        elif dx > 0:
            return([hd[0][0], hd[0][1]  + 1])
        elif dx < 0:
            return([hd[0][0], hd[0][1] - 1])

    def body(snake):
        for i in range(len(snake)-1, 0, -1):
            snake[i] = snake[i-1]
        return(snake)


#print(Grid.grid)
#input("Enter to continue: \n")

score = 0
print("Your score starts at %s" % (score))

for num in range(2):
    Object.snake[0].append(int(py_length/2))

Grid.place(Object.snake[0], Object.symbol['snake'])
print(Grid.grid)
input("Snake is placed. Enter to continue: \n")

while score < 5:

    for i in range(2):
        Object.food.append(randint(0,py_length))
    Grid.place(Object.food, Object.symbol['food'])
    print(Grid.grid)
    input("Food is placed. Enter to continue: \n")
    
    while Object.snake[0] != Object.food:
        Grid.replace(Object.snake[0], Object.symbol['cell'])
        Object.snake[0] = Snake_Movement.head(Object.snake, Object.food)
        Grid.place(Object.snake[0], Object.symbol['snake'])
        print(Grid.grid)
        input("Moved! Enter to continue: \n")

    else: 
        score += 1
        Grid.replace(Object.food, Object.symbol['snake'])
        Object.food = []
        print("Got the food! Score is %s." % (score))
        input("Enter to continue: \n")
else:
    print("Congrats, the A.I. is now working.")
    input("Enter to quit: \n")
